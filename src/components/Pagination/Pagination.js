import React, { Component } from 'react';
import './Pagination.css';
import _ from 'lodash';

const defaultProps = {
    initialPage: 1,
    pageSize: 10
}

export default class Pagination extends Component {
    constructor(props) {
        super(props)
        this.state = {
            itemsPerPage: [],
            initialPage: 1,
            pagedetails: {}
        }
    }

    componentWillMount() {
        this.setItemsPerPage(this.state.initialPage);
    }

    setItemsPerPage = (page) => {
        var { items, pageSize } = this.props
        console.log('props.data===', this.props.items);

        var pagedetails = this.getPageDetails(page, this.props.items.length, pageSize)
        var pageOfItems = items.slice(pagedetails.startIndex, pagedetails.endIndex);
        this.setState({ pagedetails })
        this.props.onChangePage(pageOfItems)
    }

    getPageDetails(currentPage, itemsLen, pageSize) {
        var { initialPage, items } = this.props
        const totalPages = items.length / pageSize;
        var startPage, endPage;
        if (totalPages <= 10) {
            startPage = initialPage;
            endPage = totalPages
        } else {
            //if total pages are more than 10
            if (currentPage <= 6) {
                startPage = 1;
                endPage = 10;
            } else if (currentPage + 4 >= totalPages) {
                startPage = totalPages - 9;
                endPage = totalPages
            } else {
                startPage = currentPage - 5
                endPage = currentPage + 4
            }
        }
        var startIndex = (currentPage - 1) * pageSize
        var endIndex = Math.min(startIndex + pageSize, itemsLen - 1);
        var pages = [...Array((endPage + 1) - startPage).keys()].map(i => startPage + i)

        return {
            totalItems: itemsLen,
            currentPage: currentPage,
            pageSize: pageSize,
            totalPages: totalPages,
            startPage: startPage,
            endPage: endPage,
            startIndex: startIndex,
            endIndex: endIndex,
            pages: pages
        };

    }

    render() {
        const { itemsPerPage, pagedetails } = this.state;
        const pageNumbersLen = this.props.items.length;
        const pageNumCount = [];
        const pager = this.state.pagedetails

        if (!pager.pages || pager.pages.length <= 1) {
            // don't display pager if there is only 1 page
            return null;
        }

        return (
            <ul className="pagination">
                < li className={pager.currentPage === 1 ? 'disabled' : ''} >
                    <button onClick={() => this.setPage(pager.currentPage - 1)}>Previous</button>
                </ li>
                {
                    pager.pages.map((page, index) =>
                        <li key={index} className={pager.currentPage === page ? 'active' : ''}>
                            <a onClick={() => this.setPage(page)}>{page}</a>
                        </li>
                    )
                }
                < li className={pager.currentPage === pager.totalPages ? 'disabled' : ''} >
                    <button onClick={() => this.setPage(pager.currentPage + 1)}>Next</button>
                </li >
            </ul>
        )


    }
}
