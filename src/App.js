
import React, { Component } from 'react';
import './App.css';
import Pagination from './components/Pagination/Pagination';
import { itemsList } from './ItemsList';
import _ from 'lodash';

export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      items: itemsList.slice(),
      data: '',
      pageofitems: []
    }
  }
  onChangePage = (pageofitems) => {
    this.setState({ pageofitems })
  }

  render() {
    return (
      <div>
        <div className="container">
          <div className="text-center">
          <h1>React - Pagination Example with logic like Google</h1>
            {_.map(this.state.pageofitems, (data, index) => {
              return (
                <div key={index}> {data}</div>
              )
            })}
            <Pagination items={this.state.items} onChangePage={this.onChangePage}></Pagination>
          </div>
        </div>
      </div>
    )
  }
}


